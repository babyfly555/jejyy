package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.Vistor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VistorRepository extends JpaRepository<Vistor, Long> {

    int countByCommunityId(long communityId);

    List<Vistor> findByCommunityIdAndCreatedAtBetween(long communityId, long start, long end);

}
