package cn.chowa.ejyy.dao.question;

import cn.chowa.ejyy.model.entity.question.Question;
import cn.chowa.ejyy.model.entity.question.QuestionOption;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionOptionRepository extends JpaRepository<QuestionOption, Long> {
}
