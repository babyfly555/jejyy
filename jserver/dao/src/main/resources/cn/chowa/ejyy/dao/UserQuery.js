function getUserInfo(leaveOffice, account){
    var sql=`
        select
        a.id,a.account,a.password,a.open_id,a.real_name,a.gender,
        a.avatar_url,a.phone,a.department_id,a.job_id,a.join_company_at,
        a.admin,a.created_at,b.subscribed,c.content
        from ejyy_property_company_user a left join ejyy_wechat_official_accounts_user b on a.union_id=b.union_id
        left join ejyy_property_company_access c on a.access_id=c.id
        where a.leave_office=:leaveOffice and a.account=:account
    `;
    return sql;
}