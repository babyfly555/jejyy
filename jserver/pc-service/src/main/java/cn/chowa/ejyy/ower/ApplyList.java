package cn.chowa.ejyy.ower;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName apply_list
 * @Description TODO
 * @Author ironman
 * @Date 15:05 2022/8/12
 */

@Slf4j
@RestController
@RequestMapping("/pc/ower")
public class ApplyList {


    /*
    * 查询业主档案列表 - YZDA
    **/
    @SaCheckRole(Constants.RoleName.YQFK)
    @VerifyCommunity(true)
    @PostMapping("/apply_list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        int pageNum = data.getPageNum();
        int pageSize = data.getPageSize();
        /*
        long communityId = data.getCommunityId();
        int replied = data.getInt("replied", false, "^1|2|3$");
        int subscribed = data.getInt("subscribed", false, "^1|0$");
        int success = data.getInt("success",false,"^0|1$");
        */
        //列表查询
        List<ObjData> temp = new ArrayList<>();
        return PagedData.of(pageNum, pageSize,temp, 0);
    }
}
