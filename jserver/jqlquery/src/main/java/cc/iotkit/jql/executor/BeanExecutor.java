package cc.iotkit.jql.executor;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

public class BeanExecutor<T> implements IExecutor<T, T> {

    public T executor(JdbcTemplate jdbcTemplate, String sql, Class<T> elementCls, Map<String, Object> params) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        List<T> list = namedParameterJdbcTemplate.query(sql, params,
                new BeanPropertyRowMapper<>(elementCls)
        );

        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

}
