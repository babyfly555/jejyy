package cn.chowa.ejyy.service;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.dao.CommunityQuery;
import cn.chowa.ejyy.dao.PropertyCompanyUserAccessCommunityRepository;
import cn.chowa.ejyy.dao.PropertyCompanyUserDefaultCommunityRepository;
import cn.chowa.ejyy.model.entity.PropertyCompanyUserAccessCommunity;
import cn.chowa.ejyy.model.entity.PropertyCompanyUserDefaultCommunity;
import cn.chowa.ejyy.vo.PostInfo;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PropertyCompanyService {

    @Autowired
    private PropertyCompanyUserAccessCommunityRepository communityRepository;
    @Autowired
    private CommunityQuery communityQuery;
    @Autowired
    private PropertyCompanyUserDefaultCommunityRepository defaultCommunityRepository;

    @Value("${wechat.pay.mch_id:}")
    private String mchId;

    public boolean verifyCommunity(long user_id, long community_id) {
        PropertyCompanyUserAccessCommunity accessCommunity = communityRepository.findByPropertyCompanyUserIdAndCommunityId(community_id, user_id);
        return accessCommunity != null;
    }

    public PostInfo getPostInfo(long userId) {
        ObjData job = communityQuery.getDepartmentJob(userId);
        List<ObjData> communityList = communityQuery.getCommunitySettingInfo(userId);
        PropertyCompanyUserDefaultCommunity defaultCommunity = defaultCommunityRepository.findByPropertyCompanyUserId(userId);

        long defaultCommunityId = defaultCommunity == null ? 0 : defaultCommunity.getCommunityId();
        if (defaultCommunity == null) {
            if (communityList.size() > 0) {
                defaultCommunityId = communityList.get(0).getLong("community_id");
                defaultCommunityRepository.save(new PropertyCompanyUserDefaultCommunity(0, userId, defaultCommunityId));
            }
        } else {
            if (communityList.size() > 0) {
                if (communityList.stream().noneMatch(
                        c -> c.getLong("community_id").longValue() == defaultCommunity.getCommunityId())) {
                    defaultCommunityId = communityList.get(0).getLong("community_id");
                    defaultCommunity.setCommunityId(defaultCommunityId);
                    defaultCommunityRepository.save(defaultCommunity);
                }
            }
        }

        return new PostInfo(job.getStr("department"),
                job.getStr("job"),
                communityList,
                defaultCommunityId,
                StrUtil.isBlank(mchId) ? 1 : 0
        );
    }

}
