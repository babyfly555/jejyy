package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ejyy_property_company_user_default_community")
public class PropertyCompanyUserDefaultCommunity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("property_company_user_id")
    private long propertyCompanyUserId;

    @JsonProperty("community_id")
    private Long communityId;

}
