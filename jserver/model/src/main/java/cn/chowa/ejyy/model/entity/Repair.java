package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_repair")
public class Repair {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("property_company_user_id")
    private Long propertyCompanyUserId;

    @JsonProperty("community_id")
    private long communityId;

    /**
     * 0 是公共区域
     */
    @JsonProperty("building_id")
    private long buildingId;

    @JsonProperty("repair_type")
    private int repairType;

    private String description;

    @JsonProperty("repair_imgs")
    private String repairImgs;

    @JsonProperty("dispose_subscribed")
    private int disposeSubscribed;

    @JsonProperty("confrim_subscribed")
    private int confrimSubscribed;

    @JsonProperty("finish_subscribed")
    private int finishSubscribed;

    @JsonProperty("allot_user_id")
    private Long allotUserId;

    @JsonProperty("alloted_at")
    private Long allotedAt;

    @JsonProperty("dispose_user_id")
    private Long disposeUserId;

    @JsonProperty("dispose_reply")
    private String disposeReply;

    @JsonProperty("dispose_content")
    private String disposeContent;

    @JsonProperty("dispose_imgs")
    private String disposeImgs;

    @JsonProperty("disposed_at")
    private Long disposedAt;

    @JsonProperty("finished_at")
    private Long finishedAt;

    @JsonProperty("merge_id")
    private Long mergeId;

    /**
     * 1业主提交; 2 客服调拨; 3维修上门; 4 维修完成; 5评价
     */
    private int step;

    /**
     * 1 - 5
     */
    private Integer rate;

    @JsonProperty("rate_content")
    private String rateContent;

    @JsonProperty("rated_at")
    private Long ratedAt;

    @JsonProperty("created_at")
    private long createdAt;

}
