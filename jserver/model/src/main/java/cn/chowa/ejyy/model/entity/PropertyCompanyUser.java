package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_property_company_user")
public class PropertyCompanyUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String account;

    private String password;

    private String openId;

    private String unionId;

    @JsonProperty("real_name")
    private String realName;

    private String idcard;

    private int gender;

    @JsonProperty("avatar_url")
    private String avatarUrl;

    private String phone;

    @JsonProperty("department_id")
    private long departmentId;

    @JsonProperty("job_id")
    private long jobId;

    @JsonProperty("access_id")
    private long accessId;

    private int admin;

    @JsonProperty("join_company_at")
    private long joinCompanyAt;

    @JsonProperty("leave_office")
    private int leaveOffice;

    @JsonProperty("created_by")
    private long createdBy;

    @JsonProperty("created_at")
    private long createdAt;
}
